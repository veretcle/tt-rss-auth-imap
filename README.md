Put this file into a directory (called auth_imap for example) into your Tiny-Tiny RSS `plugins.local` directory.

Put this configuration into your main `config.php` :

```
putenv('TTRSS_PLUGINS=auth_imap');
putenv('TTRSS_IMAP_AUTH_SERVER=<your server>:<your port>');
putenv('TTRSS_IMAP_AUTH_OPTIONS=/tls/novalidate-cert/norsh/readonly'); // options may vary depending on your needs. This example is the basic it’s working shut up please

```
