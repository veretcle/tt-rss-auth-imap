<?php
/* Requires php-imap
*  Put the following options in config.php:
*  define('IMAP_AUTH_SERVER', 'your.imap.server:port');
*  define('IMAP_AUTH_OPTIONS', '/tls/novalidate-cert/norsh');
*/

class Auth_Imap extends Auth_Base {
	private $host;

	function about() {
		return array(1.2,
			"Authenticates against an IMAP server (configured in config.php)",
			"Mortal",
			true);
	}

	function init($host) {
		$this->host = $host;

		$host->add_hook($host::HOOK_AUTH_USER, $this);
	}

	function authenticate($login, $password, $service = '') {
		if ($login && $password) {
			$imap = imap_open(
				"{".getenv('TTRSS_IMAP_AUTH_SERVER').getenv('TTRSS_IMAP_AUTH_OPTIONS')."}INBOX",
				$login,
				$password,
				OP_READONLY,
				0);

			if ($imap) {
				imap_close($imap);

				return $this->auto_create_user($login);
			}
		}

		return false;
	}

	function api_version() {
		return 2;
	}
}

?>
